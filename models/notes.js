// models/index.js

"use strict";

module.exports = function (sequelize, DataTypes) {
  var Notes = sequelize.define("notes",
    {
      data: { type: DataTypes.TEXT },
      description: DataTypes.INTEGER,
      /*coordinates: { type: DataTypes.GEOMETRY('POINT') },*/
      latitude: DataTypes.DECIMAL(9, 6),
      longitude: DataTypes.DECIMAL(9, 6)
    });

  return Notes;
};