jQuery( document ).ready(function( $ ) {

    map = L.map('map', {center:[48.783329, 2], zoom:15, zoomControl: false});
    L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
        detectRetina: true
    }).addTo(map);

    $.ajax({
        url: "/notes",
        type : 'GET',
        dataType : 'json',
        cache: false,
        timeout: 5000,
        success: function(data) {
            if (data.length > 0)
                populateMap(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
        }

    });

    markers = [];
    markersLayer = {};

    populateMap = function(data) {
        data.forEach(function(element) {
            var txt = "<b>"+element.data+"</b>"+"<br/><br/>"+element.description;
            var marker = L.marker([element.latitude, element.longitude], {
                riseOnHover:true
            }).bindPopup(txt);
            markers.push(marker);
        }, this);   
        markersLayer = L.layerGroup(markers).addTo(map); 
    }

    L.control.zoom({
        position:'topright'
    }).addTo(map);

    // LEGEND
    var legend = L.control({position: 'topleft'});

    legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'legend');
        div.innerHTML +=
        '<div style="padding:5px 0px 5px 13px;font-size:16px;font-color:black;display:block;background-color:#f4f4f4;width:170px;border-radius:5px;box-shadow:0 1px 5px rgba(0,0,0,0.65)">TRAPPES EPOPEES</div>';
    return div;
    };
    legend.addTo(map);

});
