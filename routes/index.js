// routes/index.js

var models = require('../models');
var express = require('express');
var fs = require('fs');
var parseString = require('xml2js').parseString;

var router = express.Router();

router.get('/', (req, res) => {
    res.render('home', {
        title: 'Vernaculaire'
    });
});

router.get('/notes', (req, res) => {
    models.notes.findAll().then(function (notes) {
        res.json(notes);
    });
});

router.get('/parse', (req, res) => {
    fs.readFile(__dirname + '/notes.kml', 'utf8', function (err, xml) {
        if (err) {
            return console.log('err=' + err);
        }
        //console.log(xml);
        parseString(xml, function (err, result) {
            var notesArr = result.kml.Document[0].Placemark;
            notesArr.forEach(function (element) {

                var coo = element.Point[0].coordinates[0].split(',');
                try {
                    models.notes.create({ 
                        data: element.name[0],
                        description: element.description[0],
                        latitude: coo[1],
                        longitude: coo[0]
                    });
                }
                catch (error) {
                    console.log(error);
                }
            });
        });
    });
});

module.exports = router;