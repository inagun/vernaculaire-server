// index.js
//var dotenv = require('dotenv').config();
var express = require('express');
var exphbs = require('express-handlebars');
var models = require('./models');

var routes = require('./routes/index');

var app = express();

// Layout
app.use(express.static('public'));
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

app.use('/', routes);

app.set('port', process.env.PORT || 3000);

models.sequelize.sync(/*{ force: true }*/).then(function () {
  var server = app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + server.address().port);
  });
}).catch(function (e) {
  console.log(e);
});

module.exports = app;